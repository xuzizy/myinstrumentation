#ifndef FORTIFYPATH_H
#define FORTIFYPATH_H
#include "FortifyPathNode.h"
#include "llvm/Instructions.h"
using namespace llvm;

class FortifyPath{
public:
	//FortifyPathNode* getHead();
	int getSize();
	int getId();
	FortifyPath(int pathId = 0):head(NULL),size(0),id(pathId){};
	int insert(FortifyPathNode *node);
	FortifyPathNode* find(FortifyPathNode* node);
	~FortifyPath();
		template<class Stream>
	friend Stream & operator << (Stream &out,  const FortifyPath  &path){
		FortifyPathNode* ptr = path.head;
		while(ptr){
			out << ptr<<"\n";
			ptr = ptr->next;
		}
		out << "\n";
		return out;
	}
private:
	FortifyPathNode *head;
	int size;
	int id;

};

FortifyPathNode* findInFortifyPath(FortifyPathNode* node);

//typedef bool (*Comp)(const FortifyPathNode&,const FortifyPathNode&);
//memory leak fortify report.//compare is defined in FortifyPathNode.h
//set<FortifyPathNode,Comp>* allocPoints;

int readFortifyMemoryLeakAllocPoint(string checkListFile);

/*Check if Instruction I is the fortify reported leak point*/
bool isFortifyMemoryLeakPoint(Instruction* I);

#endif