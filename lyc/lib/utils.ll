; ModuleID = 'utils.bc'
target datalayout = "e-p:32:32:32-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:32:64-f32:32:32-f64:32:64-v64:64:64-v128:128:128-a0:0:64-f80:32:32-f128:128:128-n8:16:32"
target triple = "i386-pc-linux-gnu"

@.str = private unnamed_addr constant [31 x i8] c"Buffer oveflow error detected!\00", align 4
@.str1 = private unnamed_addr constant [22 x i8] c"Safe buffer operation\00", align 1

define i32 @CheckOverflow(i8* %des_buf, i8* %src_buf) {
entry:
  %des_buf_addr = alloca i8*, align 4
  %src_buf_addr = alloca i8*, align 4
  %retval = alloca i32
  %0 = alloca i32
  %res = alloca i32
  %"alloca point" = bitcast i32 0 to i32
  store i8* %des_buf, i8** %des_buf_addr
  store i8* %src_buf, i8** %src_buf_addr
  %1 = load i8** %src_buf_addr, align 4
  %2 = call i32 @strlen(i8* %1) nounwind readonly
  %3 = load i8** %des_buf_addr, align 4
  %4 = call i32 @strlen(i8* %3) nounwind readonly
  %5 = sub i32 %2, %4
  store i32 %5, i32* %res, align 4
  %6 = load i32* %res, align 4
  %7 = icmp sgt i32 %6, 0
  br i1 %7, label %bb, label %bb1

bb:                                               ; preds = %entry
  %8 = call i32 @puts(i8* getelementptr inbounds ([31 x i8]* @.str, i32 0, i32 0))
  br label %bb2

bb1:                                              ; preds = %entry
  %9 = call i32 @puts(i8* getelementptr inbounds ([22 x i8]* @.str1, i32 0, i32 0))
  br label %bb2

bb2:                                              ; preds = %bb1, %bb
  %10 = load i32* %res, align 4
  store i32 %10, i32* %0, align 4
  %11 = load i32* %0, align 4
  store i32 %11, i32* %retval, align 4
  br label %return

return:                                           ; preds = %bb2
  %retval3 = load i32* %retval
  ret i32 %retval3
}

declare i32 @strlen(i8*) nounwind readonly

declare i32 @puts(i8*)
