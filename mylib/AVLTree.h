#ifndef AVLTREE_H
#define AVLTREE_H
#include <iostream>
using namespace std;

template<class E,class K> class AVLTree;
//template declare. See C++ primer version 4 Chinese version page 555
 template<class E,class K> ostream& operator << (ostream &out,const AVLTree<E,K> &tree) ;
 template<class E,class K> istream& operator >>  (istream &in,AVLTree<E,K> &tree); 

template <class E,class K>
struct AVLNode{
	E data;
	AVLNode<E,K> *left;
	AVLNode<E,K> *right;
	AVLNode<E,K> *parent;
	int bf; //balance factor
	int id ;

	AVLNode ():left(NULL),right(NULL),parent(NULL),bf(0){}

	AVLNode (const E& d,int ID,AVLNode<E,K> *L = NULL,AVLNode<E,K> *R = NULL,AVLNode<E,K> *P=NULL):
		data(d),left(L),right(R),parent(P) ,bf(0),id(ID){ }
};



template<class E,class K>
class AVLTree{

public:

	AVLTree():root(NULL),numberOfNodes(0){ }

	bool insert(const E& e){ 
		bool succeed = insert(root,e);
		if (succeed){
			numberOfNodes++;
		}
		return succeed;
	 }//insert a new AVLNode which satisfied AVLNode->data == e;
	
/*	AVLNode<E,K>* search(const E & e)  {
		return search(root,e);
	}*/

	AVLNode<E,K>* search(const K &k)  {
		return search(root,k);
	}

	//bool remove(K& k,const E &e){ return remote(root,k,e);}//delete element whose key is k.
															//and return the removed element through e.
	 
	friend istream& operator >> <E,K>(istream &in,AVLTree<E,K> &tree); 
	//Pay attention to the syntax
	friend ostream& operator << <E,K>(ostream &out,const AVLTree<E,K> &tree) ;

	int height()  {return height(root);};

	int getNumberOfNodes() const {return numberOfNodes;}

	void print(AVLNode<E,K> *ptr,ostream &out)const ;

	void cPrint(AVLNode<E,K> *ptr ) const;

 AVLNode<E,K>* getRoot(){
 	return root;
 }
 
private:
	AVLNode<E,K> *root;

	int numberOfNodes;

	bool insert (AVLNode<E,K> *&ptr,const E& e);//insert 


 
//	AVLNode<E,K>* search(AVLNode<E,K> *&ptr,const E & e)  ;

	AVLNode<E,K>* search(AVLNode<E,K> *&ptr,const K &k) ;
	//bool remove(AVLNode<E,K> *&ptr,K &k,const E &e);
 
	void rotateL(AVLNode<E,K> *&ptr);
	void rotateR(AVLNode<E,K> *&ptr);
	void rotateLR(AVLNode<E,K> *&ptr);
	void rotateRL(AVLNode<E,K> *&ptr);

	int height(AVLNode<E,K> *ptr);


 
};

#include "AVLTree.cpp"//In order to seperate .hpp and .cpp files

#endif