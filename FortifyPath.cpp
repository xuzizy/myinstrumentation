#include "FortifyPath.h"
#include <fstream>
#include "llvm/Support/raw_ostream.h" 
#include "GetDebugInfo.h"


int FortifyPath::getSize(){
	return size;
}

int FortifyPath::getId(){
	return id;
}

int FortifyPath::insert(FortifyPathNode *node){
	if(node == NULL)
		return -1;
	if(head == NULL){
		head = node;
	}else{
		node->next = head;
		head = node;
	}
	size ++;
	return 0;
}

FortifyPath::~FortifyPath(){
	FortifyPathNode* next = head->next;
	while(head){
		delete head;
		head = next;
		if(next)
			next = head->next;
	}
}

FortifyPathNode* FortifyPath::find(FortifyPathNode* node){
	FortifyPathNode* ptr = head;
	while(ptr){
		if(ptr->fileName == node->fileName &&
			ptr->lineNum == node->lineNum /*&&
			ptr->tag == node->tag*/)
			break;
			ptr = ptr->next;
	}
	return ptr;
}

vector<FortifyPath*>* fortifyPathes = new vector<FortifyPath*>();

FortifyPathNode* findInFortifyPath(FortifyPathNode* node){
	FortifyPathNode* ret = NULL;
	for(std::vector<FortifyPath*>::iterator iter = fortifyPathes->begin();
		iter != fortifyPathes->end(); iter++){
		if((ret = (*iter)->find(node)) != NULL){ 
			return ret;
		}
	}
	return NULL;
}
//typedef bool (*Comp)(const FortifyPathNode&,const FortifyPathNode&);
//memory leak fortify report.//compare is defined in FortifyPathNode.h
//set<FortifyPathNode,Comp>* allocPoints;

int readFortifyMemoryLeakAllocPoint(string checkListFile){
	ifstream fin(checkListFile.c_str());
	if(!fin){
		errs() << "Failed to open file " <<checkListFile << "\n";
		return -1;
	}
	 
	int count = -1;
	string fileName = "";
	int lineNum = 0;
	string tag = "";
	while(fin >> count){
		errs() <<"No "<< count << "\n";
		FortifyPath *path= new FortifyPath(count);

		while(1){
			fin>> fileName;
			if(fileName == "END_PATH") {
				errs() << *path;
				errs() <<fortifyPathes->size() << "pathes\n";
			 
				fortifyPathes->push_back(path); 
				errs() << "After push_back\n";
				break;
			}
			fin >> lineNum;
			fin >> tag;
			if(tag == "PP") continue;
		//	errs()  <<"hehe\n";
 		path->insert(new FortifyPathNode(fileName,lineNum,tag));
 		//errs()  <<"heheAfter insert\n";
		}
		
	}
	errs() << "path number = " << fortifyPathes->size()<<"\n";
/*	for(vector<FortifyPath*>::const_iterator iter = fortifyPathes->begin();
		iter != fortifyPathes->end(); iter++){
		errs() << **iter  ;
	}*/

	return 0;
}

 
