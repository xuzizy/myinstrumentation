#ifndef GETDEBUGINFO_H
#define GETDEBUGINFO_H
#include "llvm/Constants.h"
#include "llvm/Module.h"
#include "llvm/Instructions.h"
using namespace llvm;
using namespace std;
 

Constant* getGlobalString(string s, Module &M);

Constant* getLocation(Instruction* I,Module& M);

int getLineNumber(Instruction* I);

string getFilename(Instruction* I);

#endif