#include "FortifyPathMark.h"
#include <assert.h>


MultiMap markMap;

int paths[MAXPATHS][MAXNODES];

 bool
lessF(const FortifyPathNode& lhs,const FortifyPathNode& rhs){
	return ((lhs.fileName < rhs.fileName) 
		|| (lhs.fileName == rhs.fileName && lhs.lineNum < rhs.lineNum)
		/*|| (lhs.fileName == rhs.fileName && lhs.lineNum == rhs.lineNum && lhs.tag < rhs.tag)*/);
}


uint32_t MultiMap::hash(FortifyPathNode& key){

// RS Hash Function 
	unsigned int b = 378551;
	unsigned int a = 63689;
	unsigned int hash = 0; 
	
	for(int i = 0; i < key.fileName.size(); i++){
		hash = hash * a  + key.fileName[i];
		a *= b;
	} 
	     
	//printf("hash value %d\n", (a*key.lineNum)%HASHARRAY_SIZE);
	return (a*key.lineNum)%HASHARRAY_SIZE;

}


void MultiMap::insert(FortifyPathNode& key,FortifyPathMark* value){
	MapNode* newNode = new MapNode(key,value);
	unsigned int index = hash(key);
	MapNode** head = &hashArray[index];
	MapNode* prev = *head;
	MapNode* cur = prev;
	//Insert to the link in the order of key.Using the compare function to 
	//compare the key.
	
	//newNode is the first node in the link
	if(*head == NULL){ 
		*head = newNode;
		return ;
	}

	while(cur!= NULL){
		if(!lessF(cur->key,newNode->key)){
			newNode->next = cur->next;
			prev->next = newNode;
			break;
		}
		prev = cur;
		cur = cur->next;
	}

	//insert newNode in the tail
	if(cur == NULL){
		prev->next = newNode;
		newNode->next = NULL; 
	}

	//test
	/*cur = *head;
	printf("print link:\n"); 
	while(cur){
		printf("%s %d %s\n",cur->key.fileName.c_str(),cur->key.lineNum,cur->key.tagToString(cur->key.tag).c_str() );
		cur = cur->next;
		 
	}*/
}


	pair<MapNode*,MapNode*> MultiMap::find(FortifyPathNode& key){
		MapNode*first,*last,*cur;
		unsigned int index = hash(key);

		first = last = NULL;
		cur = hashArray[index];
		
		while(cur){
			if(cur->key.fileName != key.fileName || cur->key.lineNum != key.lineNum)
				cur = cur->next;
			else
				break;
		}

		if(cur != NULL){
			first = cur;
			last = first->next;
			while(last != NULL){
				if(last->key.fileName == key.fileName && last->key.lineNum == key.lineNum){
					last = last->next;
				}else{
					break;
				}
			}
		}
		return make_pair(first,last);
	}