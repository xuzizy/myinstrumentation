#!/bin/bash
# since Fortify Source Code Analyzer take same SCANID( -b SCANID )
# as one project, we should make sure different scanning has different
# SCANID, here is the result of command `date "+%Y%m%d%H%M%S"`
cat <<<`date "+%Y%m%d%H%M%S"` >ge_temp_file && make analysis
if [ -d inject_fortify ] && [ -f inject_fortify/audit.fvdl ]
then
../../../fvdlResolve/bin/fvdlParser inject_fortify/audit.fvdl
echo "checklists file generated successfully:"
cat checklists >tcas_warnings
cat checklists
else
echo "exist error when generating checklists file"
fi
