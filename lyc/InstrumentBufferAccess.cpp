#include "llvm/Pass.h"
#include "llvm/Function.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Transforms/Scalar.h"
#include "llvm/Transforms/Utils/BuildLibCalls.h"
#include "llvm/Intrinsics.h"
#include "llvm/LLVMContext.h"
#include "llvm/Module.h"
#include "llvm/Support/IRBuilder.h"
#include "llvm/Analysis/ValueTracking.h"
#include "llvm/Analysis/DebugInfo.h"
#include "llvm/Target/TargetData.h"
#include "llvm/Target/TargetLibraryInfo.h"
#include "llvm/ADT/SmallPtrSet.h"
#include "llvm/ADT/StringMap.h"
#include "llvm/ADT/Statistic.h"
#include "llvm/ADT/STLExtras.h"
#include "llvm/ADT/SmallVector.h"
#include "llvm/Support/Debug.h"
#include "llvm/Support/CFG.h"
#include "llvm/Config/config.h"
#include "llvm/Instructions.h"
#include "llvm/Instruction.h"
#include "llvm/CodeGen/ValueTypes.h"
#include "llvm/Constants.h"
#include <vector>
#include <sstream>


using namespace :: std;
using namespace :: llvm;


/**
	instrumented function: CheckOverflow(i8* des_buf, i8* src_buf);
	checks whether size_src is larger than size_dest which leads to 
	buffer overflow. Currently this is a test version, more utilities
	to add in the future
*/
void CreateInstrumentationDeclaration(Module &m){
	if(m.getFunction("CheckOverflow")){
		// Now that if we have already got such a function
		// we do not need create it again
		// TODO: there may be more functions to check, like memset ...
		return;
	}
	Function * CheckOverflow = NULL;
	std:vector<const Type*> params;
	params.push_back(PointerType::getInt8PtrTy(m.getContext()));   // first parameter, cause ArrayType::getNumElements returns uint64_t
	params.push_back(PointerType::getInt8PtrTy(m.getContext()));   // second parameter
	
	FunctionType * funcType = FunctionType::get(IntegerType::getInt32Ty(m.getContext()), params, false);

	CheckOverflow = Function::Create(funcType, GlobalValue::ExternalLinkage, "CheckOverflow", &m);
}


bool InsertAfterCallingStatement(CallInst * CI, BasicBlock::iterator insertbefore, Module &m){
	Function * callee = CI->getCalledFunction();
	Value * des_buf = NULL, * src_buf = NULL;          
	bool modified = false;
	if(callee == NULL)return false;
	if(callee->getName() == "strcpy"){
		// Currently, only consider strcpy
		Function * checkoverflow = m.getFunction("CheckOverflow");
		std::vector<Value *> params;
		des_buf = CI->getArgOperand(0);                                  // Get the pointers to the buffers
		src_buf = CI->getArgOperand(1);
		assert(des_buf && src_buf);

		params.push_back(des_buf);
		params.push_back(src_buf);

		CallInst::Create(checkoverflow, params.begin(), params.end(), "", insertbefore);

		modified = true;
		errs()<<"Inserted CheckBufferOverflow!\n";
	}

	// TODO: other type of buffer operation to be instrumented
	return modified;
}

namespace{
	struct CheckBuffer : public FunctionPass{
		static char ID;
		CheckBuffer(): FunctionPass(ID){}
		bool runOnFunction(Function & f);
		bool doInitialization(Module & m);
	};
}

bool CheckBuffer :: runOnFunction(Function & F) {
	return false;
}

bool CheckBuffer :: doInitialization(Module & M) {
	bool modified = false;
	CreateInstrumentationDeclaration(M);
	Function * checkbuffer = M.getFunction("CheckOverflow");

	assert(checkbuffer);

	for(Module :: iterator it_f = M.begin(); it_f != M.end(); it_f++) {
		if(it_f->isDeclaration())continue;
		for(Function :: iterator it_bb = it_f->begin(); it_bb != it_f->end(); it_bb++)
			for(BasicBlock :: iterator it_ins = it_bb->begin(); it_ins != it_bb->end(); it_ins++) {
				if(CallInst * inst = dyn_cast<CallInst>(it_ins)){
					it_ins++;
					InsertAfterCallingStatement(inst, it_ins, M);
					it_ins--;
					modified = true;
				}
			}
	}
	return modified;
}

char CheckBuffer :: ID = 0;
static RegisterPass<CheckBuffer> X("CheckBuffer", "Instrument code to support buffer overflow detection", false, false);
