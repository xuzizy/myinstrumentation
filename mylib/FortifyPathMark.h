#ifndef FORTIFYPATHMARK_H
#define FORTIFYPATHMARK_H

#include "../FortifyPathNode.h"
 
#include <iostream>
using namespace std;

#define MAXPATHS 100
#define MAXNODES 50
struct FortifyPathMark{
	int pathId;
	int* isPassed;
	FortifyPathMark(int id,int* passed = NULL):pathId(id),isPassed(passed){}
	template<typename Stream>
	friend Stream& operator << (Stream& OS,FortifyPathMark& mark){
		OS <<"("<< mark.pathId<<","<<*mark.isPassed<<")";
		return OS;
	}
};

typedef bool (*Comp)(const FortifyPathNode& lhs,const FortifyPathNode& rhs) ;


/*It is an array to save all the fortify reported path node marks.
paths[i][j] is a boolean value == The jth node in path i,and whether it is passed.
*/





//extern multimap <FortifyPathNode,FortifyPathMark*,Comp> markMap;
//In a link, mapNode is in the order of key;
#define HASHARRAY_SIZE 27381
struct MapNode{
	FortifyPathNode key;
	FortifyPathMark* value;
	MapNode* next;
	MapNode(FortifyPathNode& k,FortifyPathMark* val){
		key.fileName = k.fileName;
		key.lineNum = k.lineNum;
		key.tag = k.tag;
		value = val;
		next = NULL;
	}
};

struct MultiMap{
	MapNode* hashArray[HASHARRAY_SIZE];
	MultiMap(){
		for(int i = 0; i < HASHARRAY_SIZE;i ++){
			hashArray[i] = NULL;
		}
	}
	uint32_t hash(FortifyPathNode& key);
	void insert(FortifyPathNode& key,FortifyPathMark* value);
	pair<MapNode*,MapNode*> find(FortifyPathNode& key);
};


extern int paths[MAXPATHS][MAXNODES];
extern MultiMap markMap;

#endif