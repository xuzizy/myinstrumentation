#include "FortifyPathNode.h"



 bool FortifyPathNode::isAllocNode() const {
 	if (tag == MA || tag == CA || tag == RA || tag == XMA || tag == XCA || tag == XSD)
 		return true;
 	return false;
 }
 
  bool FortifyPathNode::isAllocNode() {
 	if (tag == MA || tag == CA || tag == RA || tag == XMA || tag == XCA || tag == XSD)
 		return true;
 	return false;
 }

 bool FortifyPathNode::isLeakNode()const {
 	if(tag == LK)
 		return true;
 	return false;
 }

  bool FortifyPathNode::isLeakNode()  {
 	if(tag == LK)
 		return true;
 	return false;
 }

	FortifyPathNode::Tag FortifyPathNode::stringToTag(  string strTag){
		if(strTag == "BF"){
			tag = BF;
		}else 
		if(strTag == "BT"){
			tag = BT;
		}else
		if(strTag == "MA"){
			tag = MA;
		}else
		if(strTag == "CA"){
			tag = CA;
		}else
		if(strTag == "RA"){
			tag = RA;
		}else
		if(strTag == "XMA"){
			tag = XMA;
		}else
		if(strTag == "XSD"){
			tag = XSD;
		}else
		if(strTag == "XCA"){
			tag = XCA;
		}else
		if(strTag == "LK"){
			tag = LK;
		}else
		if(strTag == "PP"){
			tag = PP;
		}else{
			tag = UNKNOWN;
		}
		return tag;
	}

	string FortifyPathNode::tagToString(const Tag t) const {
		string strTag;
		switch(t){
			case BF:	strTag = "BF";break;
			case BT:	strTag = "BT";break;
			case MA:	strTag = "MA";break;
			case CA:	strTag = "CA";break;
			case RA:	strTag = "RA";break;
			case XMA:	strTag = "XMA";break;
			case XSD: 	strTag = "XSD";break;
			case XCA:	strTag = "XCA";break;
			case LK:	strTag = "LK";break;
			case PP:	strTag = "PP";break;
			default: strTag = "Unknown tag: " + t ;
		}
		return strTag;
	}
	
		string FortifyPathNode::tagToString(const Tag t)   {
		string strTag;
		switch(t){
			case BF:	strTag = "BF";break;
			case BT:	strTag = "BT";break;
			case MA:	strTag = "MA";break;
			case CA:	strTag = "CA";break;
			case RA:	strTag = "RA";break;
			case XMA:	strTag = "XMA";break;
			case XSD: 	strTag = "XSD";break;
			case XCA:	strTag = "XCA";break;
			case LK:	strTag = "LK";break;
			case PP:	strTag = "PP";break;
			default: strTag = "Unknown tag: " + t ;
		}
		return strTag;
	}
