#ifndef LK2BLOCK_H
#define LK2BLOCK_H
#include "mem_track.h"
#include "../FortifyPathNode.h"
#define LKNUM 100
#define MANUM 100
struct MaLinkNode{
	FortifyPathNode* ma;//memory allocate node
	MaLinkNode* next;
	MaLinkNode(FortifyPathNode* alloc = NULL):ma(alloc),next(NULL){}
};

struct Lk2Ma{
	FortifyPathNode* lk;
	MaLinkNode* maList;//a set of memory allocation point
};



struct MbLinkNode{
	MemoryBlock *mb;
	MbLinkNode* next;
	MbLinkNode(MemoryBlock* memory = NULL):mb(memory),next(NULL){}
};

struct Ma2Mb{
	FortifyPathNode* ma;//memory alloc point
	MbLinkNode* mbList;
};

extern struct Lk2Ma lk2Ma[LKNUM];//a map from LK node to Ma node
extern int numOfLk;

void insertLk2Ma(FortifyPathNode* lk,FortifyPathNode* ma);
MaLinkNode* searchLk2Ma(FortifyPathNode* lk);

extern struct Ma2Mb ma2Mb[MANUM];
extern int numOfMa;

void insertMa2Mb(FortifyPathNode* ma,MemoryBlock* mb);
MbLinkNode* searchMa2Mb(FortifyPathNode* ma);
#endif