#include "mem_track.h"
#include <stdio.h>
#include "FortifyPathMark.h"
#include <fstream> 
#include <assert.h>
#include "lk2Block.h"
/*
vector<MemoryBlock>:: iterator
search(const MemoryBlock &mb){
	for(vector<MemoryBlock>::iterator iter = mbList.begin();iter != mbList.end();iter++ ){
		if(mb ==  *iter) return iter;
	}
	return mbList.end();
}

vector<MemoryBlock>:: iterator
search(i8* addr){
	for(vector<MemoryBlock>::iterator iter = mbList.begin();iter != mbList.end();iter++ ){
		if(addr >= iter->start && addr <= iter->start+iter->size && !iter->isFreed) {//iter->isFreed == false ,because if the memory is freed,
																				//we skip it and search next one.
			return iter;
		}
	}
	return mbList.end();
}
*/

 #ifdef __cplusplus
extern "C" {
#endif

//typedef multimap <FortifyPathNode,FortifyPathMark*,Comp>::iterator Iter;

/* Read Fortify analysis report and get the memory leak paths.
	We must set tag in all the path node to judge whether program
	execute in the position at runtime.And we can determine whether 
	the program pass through one of the  memory leak paths.
*/
	int pathCount = 0;

 int init(i8* fortifyReportFileName){
	//Create a multimap and a n-D array(which n is the number of paths).
	//ifstream fin(fortifyReportFileName);
 FILE* fin = fopen(fortifyReportFileName,"r");
	if(!fin){
		fprintf(stderr, "in init():Failed to open fortify report file: %s\n",fortifyReportFileName);
		exit(-1);
	}
	printf("Initilize...  \n"); 
	int nodeCount = 0;
	FortifyPathNode* allocNode = NULL;

	char fileName[100];
	int lineNum = 0;
	char tag [100];
	while(-1 != fscanf(fin,"%d",&pathCount)){
		
		nodeCount = 0;

		while(1){
			fscanf(fin,"%s",fileName);
			if(strcmp(fileName,"END_PATH") == 0){ 
				paths[pathCount][0] = nodeCount;
				//printf("nodeCount=%d\n",nodeCount );
				break;
			}
			
			fscanf(fin,"%d",&lineNum);
			fscanf(fin,"%s",tag); 
			if(strcmp(tag,"PP") == 0) continue;
			else if(strcmp(tag,"LK") == 0){
				assert(allocNode->isAllocNode());
				insertLk2Ma(new FortifyPathNode(fileName,lineNum,tag),allocNode);
			}else if(strcmp(tag,"BF") != 0 && strcmp(tag,"BT") != 0){
				allocNode = new FortifyPathNode(fileName,lineNum,tag);
			}
			nodeCount++;
		 	//fprintf(stderr,"%s %d %s   %d ",fileName,lineNum,tag,pathCount );
	 	FortifyPathMark *ptrMark = new FortifyPathMark(pathCount,&paths[pathCount][nodeCount]);
			paths[pathCount][nodeCount] = 0;
			FortifyPathNode node(fileName,lineNum,tag); 
			//markMap.hash(node);
 	 markMap.insert(node, ptrMark);
 		//printf("after markMap insert!!\n");
		}
		
	} 

 
	return 0;
}

/*
*/
int reportAtFortifyPathNode(i8*filename,int lineNum,i8* tag){ 
	FortifyPathNode node(filename,lineNum,tag);
	std::pair<MapNode*,MapNode*> ret;
 int pathId ;
	ret = markMap.find(node);
	//printf("find result: %x %x\n",(int )ret.first,(int )ret.second );
	//printf("At FortifyPathNode %s %d\n",node.fileName.c_str(),node.lineNum );	  

	//if there exist a value whose key is node.
	for(MapNode* ii = ret.first; ii != ret.second; ii = ii->next){
		pathId = ii->value->pathId;
		*ii->value->isPassed = true;//set current point(LK,BT,BF,MA) passed  s
		//
		if(node.tag == FortifyPathNode::LK){
 			MaLinkNode* maList = searchLk2Ma(&node);
 		/*	printf("The (%s,%d,%s)'s memory alloc point is\n ",
 			node.fileName.c_str(),
 			node.lineNum,
 			node.tagToString(node.tag).c_str());*/

 		while(maList){
/* 				printf("\t(%s,%d,%s)\n",maList->ma->fileName.c_str(),
 				maList->ma->lineNum,
 				maList->ma->tagToString(maList->ma->tag).c_str());*/

 				//For each memeory alloc point,we must find all of its memory block
 				//and set its state to LP.-----------------------------------------------------------
 				MbLinkNode *mbList = searchMa2Mb(maList->ma);
 				while(mbList){
 					mbList->mb->state = LP;
 					//mbList->mb->print();
 					mbList = mbList->next; 
 				}


 				maList = maList->next;
 		}
		}
	} 
	return 0;
}


int mbSetMalloc(i8* memStart,int memSize,int memNum,i8* funcname,
		i8*filename,int lineNum,i8* tag){ 
	memSize = memSize * memNum;
/*	printf("At %s line %d:  %d=%s(%d),instrument mbSetMalloc (%d,%d)\n",
		filename,lineNum,(int)memStart,funcname,memSize,(int)memStart,memSize );*/
	MemoryBlock mb(memStart,memSize);
	FortifyPathNode* ma = new FortifyPathNode(filename,lineNum,tag);
	mbTree.insert(mb); 
	AVLNode<MemoryBlock,i8*> *node = mbTree.search(memStart);
	insertMa2Mb(ma,&node->data);//Create a map from memory alloc point to memory block.
	return 1;
}

int mbSetXstrdup(i8* memStart,i8 *string,i8 *location,i8*name){
	int memSize = strlen(string)+1 ; 
	printf("At %s:  %d=%s(%d),instrument mbSetMalloc (%d,%d)\n",
		location,(int)memStart,name,memSize,(int)memStart,memSize );
	mbTree.insert(MemoryBlock(memStart,memSize));
	return 1;
}



//
int mbSetFreed(i8* memStart ,i8* location){ 
	//vector<MemoryBlock>::iterator iter = search(memStart);
	AVLNode<MemoryBlock,i8*> *node = mbTree.search(memStart);
	if (node != NULL){
		node->data.isFreed = true;
	//	printf("At  %s:  free(%d) call,instrument mbSetFreed (state = %d) ",location,(int)memStart,node->data.state);
		switch(node->data.state){
			case CREATED: node->data.state = DFREED;break;
			case LP: node->data.state = BLOAT; break;
			case USEAFTERLP: node->data.state = LNL ;break;
			default: printf("Error state %d when setfree\n", node->data.state);
		}
//		printf("to state = %d\n",node->data.state );
		return 1;
	} 
	return 0;
}


//set the memory block containing the accessedAddress isAccessed = true;
int mbSetAccessed(i8* accessedAddress,i8* location){
//	std::vector<MemoryBlock>::iterator iter = search(accessedAddress);
	AVLNode<MemoryBlock,i8*> *node = mbTree.search(accessedAddress);
	if(node != NULL){
		if(node->data.state == LP){
			node->data.state = USEAFTERLP;
		}
		 /* fprintf(stderr,"At  %s: accesse memory block [%u,%u]  \n",location,
			(unsigned)(node->data.start),
			(unsigned)(node->data.size) );    */
		return 1;
	}
	return 0;
}

string categories[100];
void getCategories( ){
	 
	string cate = "";
	std::pair<MapNode*,MapNode*> ret;
 int pathId ;
	for(int i = 0; i < 100; i++){
		categories[i] = "MAY";
	}

	//for each memory alloc point ma,
	for(int i=0;i < numOfMa; i++){
		MbLinkNode* mbList = ma2Mb[i].mbList;
		cate = "";
		while(mbList){
			if(mbList->mb->isFreed == false){
				cate = "MUST";
				break;
			}else if(mbList->mb->state == LNL && cate != "BLOAT"){
				cate = "LNL";
			}else if(mbList->mb->state == BLOAT){
				cate = "BLOAT";
			}

			mbList = mbList->next;
		}

		//find all the pathId the ma associated to.
		ret = markMap.find(*(ma2Mb[i].ma));
		for(MapNode* ii = ret.first; ii != ret.second; ii = ii->next){
			pathId = ii->value->pathId;
			categories[pathId] = cate;
		}
	}
 
}



//the return type is int because the function is going to be inserted into the end of 
//instrumented program.
//Because every callInst to be inserted into the llvm bytecode will be written in the format of
//	%i = call xxx
//So mbPrint's return type can't be void, and the default return type is i32.
int mbPrint(){
	static int count = 0;
	//Only print out once
//	fprintf(stderr,"in mbPrint()%d\n",pathCount);
	//assert(0 == 1);
	if(count) return 0;
	count++;
//	printf("Memory block list:\n");
 //	cout << mbTree<< endl;
//	mbTree.cPrint(mbTree.getRoot());
 getCategories();

	for(int pathId = 0; pathId <= pathCount; pathId++){
		printf("%d ",pathId);
		for(int i = 1; i <= paths[pathId][0] ; i++){
		//cout << paths[pathId][i] <<" ";
			if(paths[pathId][i] == 0){
				categories[pathId] = "MAY"; 
			}
		 //	printf("%d ",paths[pathId][i]);
		}
		//There will be a error when we call c_str() in klee.
		//So we delete the following line when we use klee to generate
		//test cases.And when we replay all test cases,we recover the following line
		//to print the output.
		printf("%s\n",categories[pathId].c_str());
	}
	return 0;
}

#ifdef __cplusplus
}
#endif

/*
int main(){
	mbSetMalloc(23,234);
	mbSetMalloc(23,234);

	mbSetMalloc(23,234);
	mbPrint();

	mbSetFreed(23,234);
	mbSetFreed(23,234);
	mbSetFreed(23,235);
	mbPrint();
	mbSetAccessed(250);
	mbSetAccessed(333);
	mbPrint();
	mbSetFreed(23,234); 
	mbPrint();

}*/

