#ifndef MEM_TRACK_H
#define MEM_TRACK_H
#include "AVLTree.h"
#include <iostream>
#include <stdio.h>
#include <vector>
using namespace std;
#ifdef __cplusplus
extern "C" {
#endif
	
typedef char i8;
	enum State{
		CREATED,
		DFREED,//Directly freed after create
		LP,//reach its leaking point
		USEAFTERLP,//set when accessed after LP
		BLOAT,
		LNL //LIKELY NOT LEAK.
		// BLOAT, LNL and DFREED is set when free.
		//if its state is CREATED, it is set to DFREED
		//if its state is LP, it is set to BLOAT,
		//if its state is USEAFTERLP,it is set to LNL.
	};

class MemoryBlock{
public:
	i8* start;
	int size; 
//	bool isAccessed;
	bool isFreed;

	State state;
	MemoryBlock(i8* memStart,int memSize):start(memStart),size(memSize),isFreed(false),state(CREATED){}
	
	friend ostream& operator << (ostream& out,const MemoryBlock& mb){
  out << "[" << (int)mb.start << "," << (int)(mb.size ) << ","; 
//		<< (mb.isAccessed ? "accessed" : "not accessed")
	//	<< ", "
	//	<< (mb.isFreed ? "freed" : "not freed") << "]";  

		return out;
	}

	void print(){
		printf("[%d,%d state is %d,%s] \n",(int)start,size,state, 
		 	(isFreed ? "freed" : "not freed"));
	}

	bool operator == (const MemoryBlock &mb){
		if (start == mb.start && size == mb.size) return true;
		return false;
	}

	bool operator == (const MemoryBlock &mb) const{
		if (start == mb.start && size == mb.size) return true;
		return false;
	}

	bool operator == (const i8* address){
		if(start <= address && address < start+size) return true;
		return false;
	}

	bool operator == (const i8* address) const {
		if(start <= address && address < start+size) return true;
		return false;
	}

	bool operator != (const MemoryBlock& mb){
		return !(*this == mb);
	}

	bool operator != (const MemoryBlock& mb)const{
		return !(*this == mb);
	}

	bool operator != (const i8* address){
		return !(*this == address);
	}

	bool operator != (const i8* address) const{
		return !(*this == address);
	}

	bool operator < (const MemoryBlock &mb){
		if(this->start < mb.start )
			return true;
		else if(this->start == mb.start && this->size < mb.size)
			return true;
		return false;
	}


	bool operator < (const MemoryBlock &mb) const{
		if(this->start < mb.start )
			return true;
		else if(this->start == mb.start && this->size < mb.size)
			return true;
		return false;
	}

	//memory block is [start,start+size),so it is <=
	bool operator < (const i8* address){
		if(this->start + this->size <= address) return true;
		return false;
	}
	//memory block is [start,start+size),so it is <=
	bool operator < (const i8* address)const{
		if(this->start + this->size <= address) return true;
		return false;
	}
};


//static vector<MemoryBlock> mbList;

	static AVLTree<MemoryBlock,i8*> mbTree;

int reportAtFortifyPathNode(i8*filename,int lineNum,i8* tag);

	//if  exist ,do not insert else create a MemoryBlock and insert
int mbSetMalloc(i8* memStart,int memSize,int memNum,i8* funcname,
		i8*filename,int lineNum,i8* tag);
	/* 		i8 *
		xstrdup (const i8 *string)
		{
		  return strcpy (xmalloc (strlen (string) + 1), string);
		}
*/	//xstrdup() 's parameter is not i32,so we have to use a specialized function to record its memory size.
	//mem_size = strlen(string)+1
	int mbSetXstrdup(i8* memStart,i8 *string,i8 *location,i8*name);
	//if  not exist,can't delete.
	int mbSetFreed(i8* memStart,i8* location);
	//set the memory block contains the accessedAddress isAccessed = true;
	int mbSetAccessed(i8* accessedAddress,i8* location);
	int mbPrint();

#ifdef __cplusplus
}
#endif

#endif
