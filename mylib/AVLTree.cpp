#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
template<class E,class K>
AVLNode<E,K>* AVLTree<E,K>::search(AVLNode<E,K> *&ptr,const K &k) {
		if(ptr == NULL) return NULL;

		if(ptr->data == k){//search succeed.
			return ptr;
		}else if(ptr->data < k){
			return search(ptr->right,k);
		}else {
			return search(ptr->left,k);
		}
}

template<class E,class K>
bool AVLTree<E,K>::insert(AVLNode<E,K> *&ptr,const E& e){
	AVLNode<E,K> *p = ptr,*parent = NULL,*grandparent = NULL; 
	static int id = 0;
	while(p){//travel from ptr(current root) tofind the insertion position
		if(e == p->data) return false;
		parent = p;
		if(e < p->data){
			p = p->left;
		}else{
			p = p->right;
		}
	}
	//Create new Node
	if((p = new AVLNode<E,K>(e,id++)) == NULL) {
		cerr<< "new AVLNode failed in insert()"<<endl;
		exit(-1);
	}

 
 
	//insert to empty tree
	if(parent == NULL){
		ptr = p; 
		return true;
	}

	//really insert
/*	cerr<<"before insert " << p->data<<endl;
	cerr<<*this <<endl;*/
	p->parent = parent;
	if(p->data < parent->data){
		parent->left = p;
	}else{
		parent->right = p;
	}

	while(p->parent != NULL){
		parent = p->parent;

		if(p == parent->left){	//modify parent's balance factor
			parent->bf--;
		}else{
			parent->bf++;
		}

		if(parent->bf == 0) break;
		if(parent->bf == 1 || parent->bf == -1){
			p = parent;
		}else{				//parent->bf == |2|
			if(parent->bf < 0){
				if(p->bf < 0) {
					rotateR(parent);
				}
				else{ 
					rotateLR(parent);
				}
			}else{
				if(p->bf > 0){
					rotateL(parent);
				}else{
					rotateRL(parent);
				}
			}
			break;
		}
	}//end of while
	 
	 //Here,parent is the last node rotated,which is just cut away from the tree.So we must link it back to the tree.
	if(parent->parent == NULL){		//if parent is the new root,assign it to ptr (ptr is the reference of root)
					 
		ptr = parent;
	}else {							//Else, parent has parent.You must know that the pointer from parent
		grandparent = parent -> parent;	// to its parent has already been established in the rotate functions.
									//So we can simply get grandparent by using parent->parent.
		if(parent->data < grandparent->data ){
			grandparent->left = parent;
		}else{
			grandparent->right = parent;
		}
	}

	return true;
}//insert 

template<class E,class K>
void AVLTree<E,K>::rotateLR(AVLNode<E,K> *&ptr){
	AVLNode<E,K> *left = ptr->left,*right = ptr;
	//rotate L, set child 
	ptr  = left->right;
	left->right = ptr->left;	//  ptr's left child becomes left's right child
	ptr->left = left;
	if(ptr->bf <= 0){//if ptr->bf <=0, ptr had left child,and its left child becomes left's right child
		left->bf = 0;
	}else{
		left->bf = -1;
	}

	//rotate R, set child
	right->left = ptr->right;
	ptr->right = right;
	if(ptr->bf == -1){
		right->bf = 1;
	}else{
		right->bf = 0;
	}
	ptr->bf = 0;

 	//set parent
	ptr->parent = right->parent;
	left->parent = right->parent = ptr;
	if(left->right != NULL){
		left->right->parent = left;
	}
	if(right->left != NULL){
		right->left->parent = right;
	}
}

//ptr -> bf == 2
template<class E,class K>
void AVLTree<E,K>::rotateL(AVLNode<E,K> *&ptr){
	AVLNode<E,K> *left = ptr;
	//children
	ptr = left->right;	 
	left->right = ptr->left;
	ptr->left = left;

	//parent
	ptr->parent = left->parent;
	if(left->right != NULL)	{
		left->right->parent = left;
	}
	left->parent = ptr;

	//balance factor
	ptr->bf = left->bf = 0;
}
 
//ptr->bf = -2
template<class E,class K>
void AVLTree<E,K>::rotateR(AVLNode<E,K> *&ptr){
	AVLNode<E,K> *right = ptr;
	//children
	ptr = right->left;	 
	right ->left = ptr->right;
	ptr->right = right;

	//parent
	ptr->parent = right->parent;
	if(right->left != NULL){
		right->left->parent = right;
	}
	right->parent = ptr;

	//blance factor
	ptr->bf = right->bf = 0;
}


template<class E,class K>
void AVLTree<E,K>::rotateRL(AVLNode<E,K> *&ptr){
	AVLNode<E,K> *left = ptr,*right = ptr->right;
	//rotate R, set child
	ptr  =  right->left;
	right->left = ptr->right;
	ptr->right = right;
	if(ptr->bf >= 0){
		right->bf = 0;
	}else{
		right->bf = 1;
	}

	//rotate L, set child
	left->right = ptr->left;
	ptr->left = left;
	if(ptr->bf == 1){
		left->bf = -1;
	}else{
		left->bf = 0;
	}
	ptr->bf = 0;

	//set parent
	ptr->parent = left->parent;
	left->parent = right->parent = ptr;
	if(left->right != NULL){
		left->right->parent = left;
	}
	if(right->left != NULL){
		right->left->parent = right;
	}
}


 

template<class E,class K>
ostream& operator <<  (ostream &out,const AVLTree<E,K> &tree) {
	out << "AVL Tree print(inorder): "<< endl;
	out <<"Number of Nodes : " << tree.getNumberOfNodes()<<endl;
	tree.print(tree.root,out);
	return out;
}


template<class E,class K>
void AVLTree<E,K> :: print(AVLNode<E,K> *ptr,ostream& out) const{
	if(ptr){
		print(ptr->left,out);
		 ptr->data.print();
		 printf(" id: %d\n",ptr->id);
		print(ptr->right,out);
	}
}

template<class E,class K>
void AVLTree<E,K> :: cPrint(AVLNode<E,K> *ptr ) const{
	if(ptr){
		cPrint(ptr->left );
		 ptr->data.print();
		 printf(" id: %d\n",ptr->id);
		cPrint(ptr->right );
	}
}
