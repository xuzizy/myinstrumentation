#include "llvm/Pass.h" 
#include "llvm/Function.h" 
#include "llvm/Support/raw_ostream.h"  
#include "llvm/Intrinsics.h"

#include "llvm/Module.h"  
#include "llvm/Analysis/ValueTracking.h" 

#include "llvm/Target/TargetData.h" 
#include "llvm/Target/TargetLibraryInfo.h" 
#include "llvm/ADT/SmallPtrSet.h"
#include "llvm/ADT/StringMap.h"
#include "llvm/ADT/Statistic.h" 
#include "llvm/ADT/STLExtras.h"
#include "llvm/ADT/SmallVector.h"
#include "llvm/Support/Debug.h"
#include "llvm/Support/CFG.h"
#include "llvm/Config/config.h"
#include "llvm/Instructions.h"
#include "llvm/Instruction.h"
#include "llvm/CodeGen/ValueTypes.h"
#include "llvm/Constants.h"
#include "llvm/Support/CommandLine.h"
#include <vector> 
#include <sstream> 
#include <utility>
#include <fstream>
#include "FortifyPathNode.h"
#include "FortifyPath.h"
#include "mylib/printSTL.h"
#include "GetDebugInfo.h"
using namespace llvm;
using namespace std;

cl::opt<char*> checkListFileName("checklist", cl::desc("static memleak check list file name"), cl::value_desc("fileName"));
//string checkListFileName = "checklists";

Function* createPrintfDeclaration(Module &M){
	Function *printf;
	errs() << " printf declare is not exist! I'm going to create one...\n";
	//create function type   i32 (i8*,...)
	std::vector<const Type*>FuncTy_printf_param; 
	FuncTy_printf_param.push_back(PointerType::get(IntegerType::get(M.getContext(), 8),0));  //argument: i8*
	//set function signature: int mul_add(int,int,int)
	FunctionType *FuncTy_printf = FunctionType::get( /*Result=*/IntegerType::get(M.getContext(), 32), 
							/*Params=*/FuncTy_printf_param, 
							/*isVarArg=*/true);

	printf = Function::Create(/*Type=*/FuncTy_printf,
				 /*Linkage=*/GlobalValue::ExternalLinkage,
				  /*Name=*/"printf", &M);
	errs()<< "printf function type: " << *printf->getFunctionType()<<"\n"; 
	return printf;
}

/*	 
	int mbSetMalloc(i8* memStart,int memSize,int memNum,i8* funcname,
		i8*filename,int lineNum,i8* tag);
 
	int mbSetFreed(i8* memStart,char* location);
 
	int mbSetAccessed(i8* accessedAddress,char* location);
 int init(i8*);
 int reportAtFortifyPathNode(i8*filename,int lineNum,i8* tag){ 
	int mbPrint();
	*/
void createInstrumentDeclaration(Module &M){
	Function* mbSetMalloc,*mbSetFreed,*mbSetAccessed,*mbPrint,*mbSetXstrdup;
	Function* init,*reportAtFortifyPathNode;
	errs() << "I'm going to create declaration of mbSetMalloc and mbSetFreed...\n";
	const PointerType * int8PtrTy = PointerType::getInt8PtrTy(M.getContext());
	const IntegerType *int32Ty = IntegerType::getInt32Ty(M.getContext()); 
	/*int mbSetMalloc(i8* memStart,int memSize,int memNum,i8* funcname,
		i8*filename,int lineNum,i8* tag);*/
	std::vector<const Type*>funcTy_param; 
	funcTy_param.push_back(int8PtrTy);  //argument: i8* addr
	funcTy_param.push_back(int32Ty );  //argument: i32 size
	funcTy_param.push_back(int32Ty );  //argument: i32 num
	funcTy_param.push_back(int8PtrTy );  //argument: i8* funcname
	funcTy_param.push_back(int8PtrTy);  //argument: i8* filename
	funcTy_param.push_back(int32Ty );	//argument: int lineNum
	funcTy_param.push_back(int8PtrTy);//argument: i8* tag
	FunctionType *funcTyInsert = FunctionType::get( /*Result=*/int32Ty, 
							/*Params=*/funcTy_param, 
							/*isVarArg=*/false); 
	mbSetMalloc = Function::Create(/*Type=*/funcTyInsert,
				 /*Linkage=*/GlobalValue::ExternalLinkage,
				  /*Name=*/"mbSetMalloc", &M);


	//i32 mbSetFreed(i8*,i8*)
	funcTy_param.pop_back();
	funcTy_param.pop_back();
	funcTy_param.pop_back();
	funcTy_param.pop_back();
	funcTy_param.pop_back();
	funcTy_param.pop_back();
	funcTy_param.push_back(int8PtrTy );  //argument: i8* location
	FunctionType *funcTySetFreed= FunctionType::get( /*Result=*/int32Ty, 
							/*Params=*/funcTy_param, 
							/*isVarArg=*/false); 
	mbSetFreed = Function::Create(/*Type=*/funcTySetFreed,
				 /*Linkage=*/GlobalValue::ExternalLinkage,
				  /*Name=*/"mbSetFreed", &M);

	//i32 mbSetAccessed(i8*,i8*)
	mbSetAccessed = Function::Create(funcTySetFreed,GlobalValue::ExternalLinkage,"mbSetAccessed",&M);

	//i32 init(i8*);
	funcTy_param.pop_back();
	FunctionType *funcTyInit = FunctionType::get(int32Ty,funcTy_param,false);
	init = Function::Create(funcTyInit,GlobalValue::ExternalLinkage,"init",&M);

	//int reportAtFortifyPathNode(i8*,int,i8* ){ 
	funcTy_param.push_back(int32Ty);
	funcTy_param.push_back(int8PtrTy);
	FunctionType *funcTyReport = FunctionType::get(int32Ty,funcTy_param,false);
	reportAtFortifyPathNode = Function::Create(funcTyReport,GlobalValue::ExternalLinkage,"reportAtFortifyPathNode",&M);

	//i32 mbSetXstrdup(i8*,i8*,i8*,i8*)
	funcTy_param.pop_back();
	funcTy_param.pop_back();
	funcTy_param.push_back(int8PtrTy);
	funcTy_param.push_back(int8PtrTy);
	funcTy_param.push_back(int8PtrTy);
	FunctionType *funcTySetXstrdup = FunctionType::get (int32Ty,funcTy_param,false);
	mbSetXstrdup = Function::Create(funcTySetXstrdup,GlobalValue::ExternalLinkage,"mbSetXstrdup",&M);

	//i32 mbPrint();
	mbPrint = Function::Create(FunctionType::get(int32Ty,false),
								GlobalValue::ExternalLinkage,
								"mbPrint",&M);

	errs()<< "reportAtFortifyPathNode  function type: " << *reportAtFortifyPathNode->getFunctionType()<<"\n"; 
	errs()<< "init  function type: " << *init->getFunctionType()<<"\n"; 
 errs()<< "mbSetFreed  function type: " << *mbSetFreed->getFunctionType()<<"\n"; 
	errs()<< "mbSetMalloc function type: " << *mbSetMalloc->getFunctionType()<<"\n"; 
	errs()<< "mbSetXstrdup function type: " << *mbSetXstrdup->getFunctionType()<<"\n"; 
	errs()<< "mbSetAccessed function type: " << *mbSetAccessed->getFunctionType()<<"\n"; 
	errs()<< "mbPrint function type: " << *mbPrint->getFunctionType()<<"\n";  
}


void insertPrintf(string s,vector<Value*> &varArgs,Function *printf,Instruction* insertBefore,Module& M) {
	//constructor the argument string of printf().
	//the argument is a Global String variable
	std::vector<Value *>printArgs; //the fact arguments to instrument printf(i8*)
	Constant* globalString = getGlobalString(s,M);
	//errs()<<"str_init: "<<*str_init << "\n";
	//errs()<<"pointer to the global string: "<<*(gep)<<"\n";
	printArgs.push_back(globalString);//add the pointer to vector printArgs which is used to CallInst::Create(...)
	for (std::vector<Value*>::iterator i = varArgs.begin(); i != varArgs.end(); i++){
		printArgs.push_back(*i);
	}
	CallInst::Create(printf,  printArgs.begin(),printArgs.end(),"",insertBefore); 
}

//return whether is modified the programe.
bool insertCall(CallInst *CI,BasicBlock::iterator  insertBefore,Module& M){ 
 	Function *Callee = CI->getCalledFunction(); 
 	if(Callee == NULL) return false;
 	bool modified = false; 
 	bool foundAlloc = false;
 	Value *mem_size = NULL;
 	Value *mem_num = NULL;
 	Value *xstrdupArg = NULL;
 	Value * one = ConstantInt::get (IntegerType::getInt32Ty(M.getContext()),1);
 	//the function name will be changed after the c++ code compile to bytecode
 	//from mbSetMalloc to _Z8mbSetMallocPci
 	Function *mbSetMalloc = M.getFunction("mbSetMalloc"); 
 	assert(mbSetMalloc  );
 	static FortifyPathNode node;
 	if(Callee->getName() == "malloc"){
 		foundAlloc = true;
 		mem_size = CI->getArgOperand(0); 
 		mem_num = one;
 		node.tag = FortifyPathNode ::MA;
 	}else if(Callee->getName() == "realloc"){
 		foundAlloc = true;
 		mem_size = CI->getArgOperand(1); 
 		mem_num = one;
 		node.tag = FortifyPathNode ::RA;
 	}else if(Callee->getName() == "calloc"){
 		foundAlloc = true;
 	 	mem_size =   CI->getArgOperand(1); 
 	 	mem_num = CI->getArgOperand(0) ;
 	 	node.tag = FortifyPathNode ::CA;
 	}else if(Callee->getName() =="xmalloc"){
 		foundAlloc = true;
 		mem_size = CI->getArgOperand(0); 
 		mem_num = one;
 		node.tag = FortifyPathNode ::XMA;
 	}else if(Callee->getName() =="xstrdup"){//char * xstrdup(const char* string),
/* 		char *
		xstrdup (const char *string)
		{
		  return strcpy (xmalloc (strlen (string) + 1), string);
		}
*/
 		foundAlloc = true;
 		xstrdupArg = CI->getArgOperand(0); //mem_size == strlen(string)+1 
 		node.tag = FortifyPathNode::XSD;
 	}else if(Callee->getName() =="xcalloc"){
 		foundAlloc = true;
 	 	mem_size = CI->getArgOperand(1); 
 	 	mem_num = CI->getArgOperand(0) ;
 	 	node.tag = FortifyPathNode::XCA;
 	}

 	node.lineNum = getLineNumber(CI);
 	node.fileName = getFilename(CI);
 	if(foundAlloc && (node.lineNum == 0 || node.fileName == "")){
 		errs() << Callee->getName() <<" can't get debug infomation!\n"
 		<<node.lineNum << ","<< node.fileName<<"\n";
 	}
 	
 	/*If Callee is the target memory allocation point,
 	insert a function call to record the memory block and fortify path.
 	*/
 /*	if(foundAlloc && node.fileName == "cat.c")
 		errs() << "memory alloc point " << node<< "\n";*/
 	if(foundAlloc && findInFortifyPath(&node)){
 		errs()<<"foundAlloc: "<<node<<"\n";
			std::vector<Value*>args;
			//argument 1
			args.push_back(dyn_cast<Instruction>(CI));//mem address
			if(node.tag == FortifyPathNode::XSD){	//xstrdup arguments is char*
				args.push_back(xstrdupArg);
			}else{
				//argument 2
				args.push_back(mem_size);//mem size
				//argument 3
				args.push_back(mem_num);//mem num
			}
				////argument 4
			args.push_back(getGlobalString(Callee->getName(),M));//name
			//argument 5 6 7 is fileName,lineNum and tag
			args.push_back(getGlobalString(node.fileName,M));
			args.push_back(ConstantInt::get (IntegerType::getInt32Ty(M.getContext()),node.lineNum));
			args.push_back(getGlobalString(node.tagToString(node.tag),M));
		//	errs()<<"mem_size type : " << *args[1]->getType() << "\n";
		//	insertPrintf(" Find a %d=malloc(%d) call at line %d\n",args,printf,insertBefore,M);
		//mbSetMalloc(i8*,i32,i32);
			if(node.tag == FortifyPathNode::XSD){
			//Because xstrdup 's parameter is different from other memory allocation function,
			//we create a specilized record function for xstrdup.
				CallInst::Create(M.getFunction("mbSetXstrdup"),args.begin(),args.end(),"",insertBefore);
			}else{
				//Other malloc function
				CallInst::Create(mbSetMalloc,  args.begin(),args.end(),"",insertBefore);   
			}
			errs()<<"After insert call\n";
			modified = true;//instrument one instruction-- mbSetMalloc
	}else if(Callee->getName() == "free"){
		Function *mbSetFreed = M.getFunction("mbSetFreed"); 
		std::vector<Value*>args;  //argument of printf and mbSetFreed(i8*)
		//cast the free(ptr) argument to i8*.Because the free argument can be any pointer type,
		//but the mbSetFreed can only accept i8*,so it must insert a cast instruction.
		CastInst *ptrInt8Arg = CastInst::CreateZExtOrBitCast(CI->getArgOperand(0),//src value
									PointerType::getInt8PtrTy(M.getContext()),//dest type
									"",//instruction name
									insertBefore);//insert position
		args.push_back(ptrInt8Arg);
		args.push_back(getLocation(insertBefore,M));
		//insertPrintf("Find a free(%d) call at line %d\n",args,printf,insertBefore,M);
		//errs()<<"before insert: "<<*insertBefore<<"\n";
	 	CallInst::Create(mbSetFreed,args.begin(),args.end(),"",insertBefore); 
	 //	errs()<<"after insert: "<<*insertBefore<<"\n";
	  
		modified = true;//instrument 2 instructions, cast and call mbSetFreed
	}else if(isa<UnreachableInst>(insertBefore)){
		//All function call instruction similar to exit() is the exit positon of the program,we can insert the 
		//mbPrint function before exit call to print the result of the memory leak detection.
		Function *mbPrint = M.getFunction("mbPrint");
		//errs()<<"Find exit position: call "<<Callee->getName()<<"\n"; 
		//insert mbPrint before exit instruction.
		//it is the summary of the dynamic memory leak detection
		CallInst::Create(mbPrint,"", --insertBefore); 
		modified = true;
	}
	 return modified;
 }


namespace { 
	struct MyInstrumentation : public FunctionPass {
		static char ID; 
		MyInstrumentation() : FunctionPass(ID) {} 
		bool runOnFunction(Function &F); 
		bool doInitialization(Module &M); 
	}; 
}

bool MyInstrumentation::runOnFunction(Function &F) { 
	//errs() << "MyInstrumentation: "; 
	//errs().write_escaped(F.getName()) << '\n'; 
	return false; 
}

 

bool MyInstrumentation::doInitialization(Module &M) { 
	string checklistfile (checkListFileName);
	errs() << "Fortify report file name = "<< checklistfile << "\n";
	if (-1 == readFortifyMemoryLeakAllocPoint(checklistfile)){
		exit(-1);
	}

	//------------test getTargetPoint(M)
 	bool modified = false; 
	//create function declaration of printf ,mbSetMalloc and mbSetFreed 
	createInstrumentDeclaration( M);
	Function *mbSetAccessed = M.getFunction("mbSetAccessed");
	Function *mbPrint = M.getFunction("mbPrint"); 
 Function *main = M.getFunction("main");
 Function *init = M.getFunction("init");
 Function *reportAtFortifyPathNode = M.getFunction("reportAtFortifyPathNode");
 FortifyPathNode	node;
 FortifyPathNode* ret = NULL;
 int prevLineNum = 0;
 string prevFileName = "";
 	//main->viewCFG();
	//assert the function pointer
	assert(mbSetAccessed && mbPrint && init);

 	/*
			Insert init function call to read the fortify report and initialize 
			the data structures for fortify path node's passing tag.
			@params: fortifyReportFileName
	*/
			std::vector<Value*>args;
  	args.push_back(getGlobalString(checklistfile,M));
  	CallInst::Create(init,args.begin(),args.end(),"",main->getEntryBlock().begin());

	/*
			Insert mbPrint at the end of main,it is the summary of the dynamic 
			memory leak detection
	*/
			if(ReturnInst* returnInst = dyn_cast<ReturnInst>(main->back().getTerminator())){
			 	CallInst::Create(mbPrint,"",returnInst);
			}
	


 	//travel the whole program to find insert point. Find the function call  malloc()) and insert  printf function.
	for (Module::iterator f = M.begin(), E = M.end(); f != E; ++f) { 
		Function &F = *f; 
	 //errs()<<"function name: " << F.getName() <<"\n";
		if (F.isDeclaration()) continue;  
		
		for(Function::iterator BB = f->begin(); BB != f->end(); BB++)
		for (BasicBlock::iterator I = BB->begin(), E = BB->end(); I != E;I++){ 	

					//node.tag = FortifyPathNode::LK;
					node.lineNum = getLineNumber(I);
					node.fileName = getFilename(I);

			if((prevLineNum != node.lineNum || prevFileName != node.fileName) 
				&&((ret = findInFortifyPath(&node)) != NULL)){ 
				/*Insert a call  to report that it is a potential memory leak 
				 *point reported by Fortify.The function call inserted 
				 *need know which memory leak path it belongs to. 
				 *prevLineNum and prevFileName is to avoid replicate inserting call.
				 */

				 prevLineNum = node.lineNum;
				 prevFileName = node.fileName;
				 std::vector<Value*> args;
				 args.push_back(getGlobalString(ret->fileName,M));
				 args.push_back(ConstantInt::get (IntegerType::getInt32Ty(M.getContext()),ret->lineNum));
				 args.push_back(getGlobalString(ret->tagToString(ret->tag),M));
				 CallInst::Create(reportAtFortifyPathNode,args.begin(),args.end(),"",I);
			}

			//errs()<<"instruction:" <<*I<<"\n";
			if(CallInst *CI = dyn_cast<CallInst>(I)){
				//We must insert the mbSetMalloc function after the malloc  
				//because we want the return value of malloc.
				I++;
				modified = insertCall(CI,I,M); 	
				I--;
				//errs()<<"Find call after\n";
				  
			}else if (I->mayReadFromMemory() || I->mayWriteToMemory()){
				std::vector<Value*> args;	  
				//errs()<<"Find memory use \n";
				//insert print in every instruction which access heap memory
				for(User::op_iterator op_i = I->op_begin() ,e = I->op_end(); op_i != e; op_i++){ 
					if(Value* operand = dyn_cast<Value>(op_i)){
						if(operand->getType()->isPointerTy()){ 
							CastInst *ptrInt8Arg = CastInst::CreateZExtOrBitCast(operand,//src value
									PointerType::getInt8PtrTy(M.getContext()),//dest type
									"",//instruction name
									I);//insert before I
							args.push_back(ptrInt8Arg); 
							args.push_back(getLocation(I,M));
						  	//errs()<<"IN line "<<*args[1] <<" : "<<*I << "args num: "<<args.size()<<"\n";
							CallInst::Create(mbSetAccessed,args.begin(),args.end(),"",I); //insert mbSetAccessed(i8*addr,char* location)
							//errs()<<"IN line "<<*args[1] <<" : "<<*I << "args num: "<<args.size()<<"\n";
							args.pop_back();
							args.pop_back();
							
						}
					}
				} 
			}


		}

		 
	 
	}  
	
	return modified;
}

char MyInstrumentation::ID = 0;
 static RegisterPass<MyInstrumentation> X("MyInstrumentation", "Dynamically Memory Leak Checking Pass", false, false); 
