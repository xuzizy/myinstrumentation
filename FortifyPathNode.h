#ifndef FORTIFYPATHNODE_H
#define FORTIFYPATHNODE_H
#include <string>
using namespace std;
struct FortifyPathNode
{
	 
	string fileName;//Not include the directory of file
	int lineNum;
	enum Tag{
		BF,//Branch True
		BT,//Branch False
		MA,//malloc
		CA,//calloc
		RA,//realloc
		XMA,//xmalloc
		XCA,//xcalloc
		XSD,//xstrdup
		LK,//Memory leak point
		PP,//normal path point
		UNKNOWN
	};
	Tag tag;
	FortifyPathNode *next;

	//bool operator < (const FortifyPathNode & n)   {
	//	return (fileName < n.fileName) || ((fileName == n.fileName) && lineNum < n.lineNum);
 

FortifyPathNode(string f="" ,int line=0 ,  string t="" ):
		fileName(f),lineNum(line),next(NULL){
		tag = stringToTag(t);
	}


 bool isAllocNode() const ;
 
  bool isAllocNode() ;

 bool isLeakNode()const ;

  bool isLeakNode();

	Tag stringToTag(  string strTag);
	string tagToString(const Tag t) const ;
	
		string tagToString(const Tag t);
	template<class Stream>
	friend Stream & operator << (Stream &out,   FortifyPathNode  &node){
		out << node.fileName << " "<<node.lineNum << " " << node.tagToString(node.tag);
		return out;
	}

 	template<class Stream>
	friend Stream & operator << (Stream &out,  const FortifyPathNode   *node){
		if(node)
			out << node->fileName << " "<<node->lineNum 
							<< " " << node->tagToString(node->tag);
		return out;
	} 
};//end of definition FortifyPathNode

inline bool
compare(const FortifyPathNode& lhs,const FortifyPathNode& rhs){
	return ((lhs.fileName < rhs.fileName) 
		|| (lhs.fileName == rhs.fileName && lhs.lineNum < rhs.lineNum)
		/*|| (lhs.fileName == rhs.fileName && lhs.lineNum == rhs.lineNum && lhs.tag < rhs.tag)*/);
}


#endif