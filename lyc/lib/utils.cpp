/*
 * =====================================================================================
 *
 *       Filename:  utils.cpp
 *
 *    Description:  In this file, we inplement the dynamic buffer oveflow
 *    		    detection functions.
 *
 *        Version:  1.0
 *        Created:  12/09/2013 02:56:50 PM
 *       Revision:  none
 *       Compiler:  llvm-gcc
 *
 *         Author:  Yongchao Li
 *        Company:  NJU 
 *
 * =====================================================================================
 */


#include <stdio.h>
#include <string.h>
#include <utils.h>

extern "C" int CheckOverflow(i8 * des_buf, i8 * src_buf) {
	int res = strlen(src_buf) - strlen(des_buf);
	if(res > 0)printf("Buffer oveflow error detected!\n");
	else
		printf("Safe buffer operation\n");
	return res;
}
