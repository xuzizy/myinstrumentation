#include "lk2Block.h"
#include <assert.h>

struct Lk2Ma lk2Ma[LKNUM];//a map from LK node to Ma node
int numOfLk = 0;

struct Ma2Mb ma2Mb[MANUM];
int numOfMa = 0;

void insertLk2Ma(FortifyPathNode* lk,FortifyPathNode* ma){
	assert(lk->isLeakNode());
	for(int i = 0; i < numOfLk; i++){
		assert(lk2Ma[i].lk);
		//insert ma to the link list
		if(lk->fileName == lk2Ma[i].lk->fileName &&
			lk->lineNum == lk2Ma[i].lk->lineNum){
			//Determined that the ma point has not been saved yet.
				MaLinkNode* ptr = lk2Ma[i].maList;
				while(ptr){
					if(ptr->ma->fileName == ma->fileName &&
						ptr->ma->lineNum == ma->lineNum)
						return ;//ma has been saved already.  
					ptr = ptr->next;
				}

			//insert to the link list 
				ptr = lk2Ma[i].maList;
				lk2Ma[i].maList = new MaLinkNode(ma);
				lk2Ma[i].maList->next = ptr;
				return ;			 
		}
	}

	//Didn't not find lk in lk2Ma,we must insert lk firstly.
	if(numOfLk >= LKNUM){
		fprintf(stderr, "lk point is full in insertLk2Ma\n");
		exit(-1);
	}
	lk2Ma[numOfLk].lk = lk;
	lk2Ma[numOfLk].maList = new MaLinkNode(ma);
	numOfLk++;
}

MaLinkNode* searchLk2Ma(FortifyPathNode* lk){
	for(int i = 0; i < numOfLk; i++){
		if(lk->fileName == lk2Ma[i].lk->fileName &&
			lk->lineNum == lk2Ma[i].lk->lineNum){
			return lk2Ma[i].maList;
		}
	}
	return NULL;//search failed.
}

void insertMa2Mb(FortifyPathNode* ma,MemoryBlock* mb){
	assert(ma->isAllocNode());
	for(int i = 0; i < numOfMa; i++){
		assert(ma2Mb[i].ma);
		//insert mb to the link list
		if(ma->fileName == ma2Mb[i].ma->fileName &&
			ma->lineNum == ma2Mb[i].ma->lineNum){
			//Determined that the mb point has not been saved yet.
				MbLinkNode* ptr = ma2Mb[i].mbList;
				while(ptr){
					if((int)ptr->mb->start == (int)mb->start &&
						ptr->mb->size == mb->size)
						return ;//mb has been saved already.  
					ptr = ptr->next;
				}

			//insert to the link list 
				ptr = ma2Mb[i].mbList;
				ma2Mb[i].mbList = new MbLinkNode(mb);
				ma2Mb[i].mbList->next = ptr;
				return ;			 
		}
	}

	//Didn't not find ma in ma2Mb,we must insert ma firstly.
	if(numOfMa >= LKNUM){
		fprintf(stderr, "ma point is full in insertMa2Mb\n");
		exit(-1);
	}
	ma2Mb[numOfMa].ma = ma;
	ma2Mb[numOfMa].mbList = new MbLinkNode(mb);
	numOfMa++;
}


MbLinkNode* searchMa2Mb(FortifyPathNode* ma){
		for(int i = 0; i < numOfMa; i++){
		if(ma->fileName == ma2Mb[i].ma->fileName &&
			ma->lineNum == ma2Mb[i].ma->lineNum){
			return ma2Mb[i].mbList;
		}
	}
	return NULL;//search failed.
}