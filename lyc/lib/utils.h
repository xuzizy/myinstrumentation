/*
 * =====================================================================================
 *
 *       Filename:  utils.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  12/09/2013 03:18:05 PM
 *       Revision:  none
 *       Compiler:  llvm-gcc
 *
 *         Author:  Yongchao Li 
 *        Company:  NJU
 *
 * =====================================================================================
 */

#include <string.h>
#include <stdio.h>
typedef char i8;
/**
 * Given the start address of source buffer and destination buffer
 * returns whether this access action may incur buffer overflow
 * returns positive integer to indicate buffer overflow, non-positive
 * to safe.
 */


extern "C" int CheckOverflow(i8 * des_buf, i8 * src_buf);
